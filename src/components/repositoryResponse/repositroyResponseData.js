import React, {useEffect, useState} from 'react';
import axios from 'axios';
import './repositoryResponseData.css'

const ResponseDataList = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios.get('http://localhost:8080/getRepositoryData/6527683')
            .then(response => {
                setData(response.data);
                setLoading(false)
            })
            .catch(error => {
                console.error('Error fetching data:', error);
                setLoading(false)
            });
    }, []);

    return (
        <div>
            <h2>Application Admin</h2>
            {loading ? (
                <p>Loading...</p>
            ) : (
                <table className="repositoryDetailsTable">
                    <thead>
                    <tr>
                        <th>Application Name</th>
                        <th>Repository Type</th>
                        <th>Java Version</th>
                        <th>Spring Version</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((item, index) => (
                        <tr key={index}>
                            <td>{item.projectName}</td>
                            <td>{item.repositoryType}</td>
                            <td>{item.javaVersion}</td>
                            <td>{item.springVersion}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            )}
        </div>
    );
};

export default ResponseDataList;
